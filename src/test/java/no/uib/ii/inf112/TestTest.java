package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if (text == null) {
				return " ".repeat(width);
			}
			if (width < 0) {
				throw new IllegalArgumentException("Width must be non-negative.");
			}

			if (width < text.length()) {
				StringBuilder toReturn = new StringBuilder();
				char[] chars = text.toCharArray();
				for (int i=0; i < text.length() / width; i++) {
					for (int j = 0; j < width; j++) {
						toReturn.append(chars[(i*width) + j]);
					}
					toReturn.append("\n");
				}
				toReturn.deleteCharAt(toReturn.length()-1);
				return toReturn.toString();


//				int currentLine = 1;
//				StringBuilder str = new StringBuilder(text);
//				while(str.length()/width > currentLine) {
//					str.insert(width * currentLine + currentLine - 1, "\n");
//					currentLine += 1;
//				}
//				return str.toString();

			}

			int extra = (width - text.length());
			return " ".repeat(extra/2) + text + " ".repeat(extra / 2 + extra % 2);
		}
		public String flushRight(String text, int width) {
			int extra = width - text.length();
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = width - text.length();
			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("   ", aligner.center(null, 3));
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("  A   ", aligner.center("A", 6));
		assertEquals("f\no\no", aligner.center("foo", 1));
		assertEquals("fo\noo\noo", aligner.center("fooooo", 2));
		assertEquals("foo\nooo\nooo", aligner.center("foooooooo", 3));
		assertEquals("foo\nooo\noo", aligner.center("fooooooo", 3));
	}

	@Test
	void testLongerThanWidth() {
		assertEquals("f\no\no", aligner.center("foo", 1));
	}

	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("    foo", aligner.flushRight("foo", 7));
	}

	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("foo    ", aligner.flushLeft("foo", 7));
	}
}
