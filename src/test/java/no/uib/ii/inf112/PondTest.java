package no.uib.ii.inf112;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.PondObject;
import no.uib.ii.inf112.pond.impl.Duck;
import no.uib.ii.inf112.pond.impl.Duckling;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PondTest {

    Pond pond;

    @BeforeEach
    void init() {
        pond = new Pond(1280, 720);

    }

//    @Test
//    void doesOneStep() {
//        pond.step();
//    }

    @Test
    void testIntervalSize() {
        int interval = 25;

        assertEquals(1, pond.objs.size()); // "🦆"

        stepInterval(interval);
        assertEquals(2, pond.objs.size()); // "🦆🐤"

        stepInterval(interval);
        assertEquals(3, pond.objs.size()); // "🦆🐤🐤"

        stepInterval(interval);
        assertEquals(4, pond.objs.size()); // "🦆🐤🐤🐤"

        // Before
        // Interval size: Excepted 25, Actual 12
        // --> changed variable 10 to 25
        // --> changed (stepCount++ > 25) to (++stepCount >= 25)
        // After
        // Interval size: Excepted 25, Actual 25


        // Expected: 1-2-3-4-...
        // Actual: 1-2-4-8-16-32-64-...
    }

    private void stepInterval(int interval) {
        for (int i = 0; i < interval; i++) {
            pond.step();
        }
    }

    @Test
    void testDuck() {
        PondObject duck = pond.objs.get(0);
        assertEquals(25, duck.getSize());
        assertEquals(0, duck.getX());
        assertEquals(0, duck.getY());
        duck.step(pond);
        assertEquals(-1, duck.getX());
        assertEquals(0, duck.getY());

    }

    @Test
    void testDucklingSize() {
        stepInterval(25);
        PondObject duckling = pond.objs.get(1);
        assertEquals(5, duckling.getSize());
        duckling.step(pond);
        assertEquals(6, duckling.getSize());
    }

}
