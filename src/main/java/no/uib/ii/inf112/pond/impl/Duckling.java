package no.uib.ii.inf112.pond.impl;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.Position;

public class Duckling extends Duck {
	public Duckling(Position pos, double size) {
		super(pos, size);
	}

	@Override
	public void step(Pond pond) {
		pos.move(-1, 0);
		size++;
	}
}
